import React, { Component } from "react";
import { fetchNutrition } from "../actions/nutrition";
import { connect } from "react-redux";
import { Input } from "semantic-ui-react";
import 'semantic-ui-css/semantic.min.css';


class SearchBar extends Component {
  render() {
    const formatString = (entry, character) => {
      if (entry !== "") {
        return entry
          .trim()
          .toLowerCase()
          .split(" ")
          .join(character);
      }
    };
    return (
      <>
        <div className="foodSearch">
          <Input action={{ icon: 'search', onClick: event => {
              this.props.fetchNutrition(
                formatString(this.state.foodSearch, "20%"),
                this.props.type
              );
            }}}
            placeholder="Search for Food"
            onChange={(event) => this.setState({foodSearch: event.target.value})}
          /> 
        </div>
      </>
    );
  }
}

const mapStateToProps = state => {
  return { food: state.food };
};
const mapDispatchToProps = {
  fetchNutrition
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchBar);
