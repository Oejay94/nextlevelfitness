import React, { Component } from "react";

import { Line, Bar, Pie } from "react-chartjs-2";
//import { Grid } from "semantic-ui-react";

const userData = {
  macronutrients: {},
  stepsTaken: {}
};

const pieData = {
  labels: ["Carbs", "Protein", "Fat"],
  datasets: [
    {
      data: [20, 50, 100],
      backgroundColor: ["#FF6389", "#36A2EB", "#FFCE56"],
      hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56"]
    }
  ]
};

const barData = {
  labels: [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday"
  ],
  datasets: [
    {
      label: "Calories Burned",
      backgroundColor: "rgba(255,99,132,0.2)",
      borderColor: "rgba(255,99,132,1)",
      borderWidth: 5,
      hoverBackgroundColor: "rgba(255,99,132,0.4)",
      hoverBorderColor: "rgba(255,99,132,1)",
      data: [2500, 1800, 2000, 2100, 3000, 3100, 2800]
    },
    {
      label: "Calories Consumed",
      backgroundColor: "rgba(50,50,50,0.2)",
      borderColor: "rgba(255,99,132,1)",
      borderWidth: 1,
      hoverBackgroundColor: "rgba(255,99,132,0.4)",
      hoverBorderColor: "rgba(255,99,132,1)",
      data: [1800, 1900, 2000, 1850, 3333, 2000, 1500]
    }
  ]
};

const secondBarData = {
  labels: [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday"
  ],
  datasets: [
    {
      label: "Steps Taken Per Day",
      background: "rgba(255,99,132,0.2)",
      borderColor: "rgba(255,99,132,1)",
      borderWidth: 5,
      hoverBackgroundColor: "rgba(255,99,132,0.4)",
      hoverBorderColor: "rgba(255,99,132,1)",
      data: [2500, 1800, 2000, 2100, 3000, 3100, 2800]
    }
  ]
};

const lineData = {
  labels: [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday"
  ],
  datasets: [
    {
      label: "Total Work Capacity",
      fill: true,
      lineTension: 0.01,
      backgroundColor: "rgba(75,192,192,0.4)",
      borderColor: "rgba(75,192,192,1)",
      borderCapStyle: "butt",
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: "miter",
      pointBorderColor: "rgba(75,192,192,1)",
      pointBackgroundColor: "#fff",
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: "rgba(75,192,192,1)",
      pointHoverBorderColor: "rgba(220,220,220,1)",
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: [59, 40, 10, 13, 66, 90, 100]
    },
    {
      label: "Energy Expenditure",
      fill: true,
      lineTension: 0.01,
      backgroundColor: "rgba(75,192,192,0.4)",
      borderColor: "rgba(75,192,192,1)",
      borderCapStyle: "butt",
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: "miter",
      pointBorderColor: "rgba(75,192,192,1)",
      pointBackgroundColor: "#fff",
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: "rgba(75,192,192,1)",
      pointHoverBorderColor: "rgba(220,220,220,1)",
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: [23, 40, 100, 13, 66, 90, 100]
    }
  ]
};

class Charts extends Component {
  render() {
    return (
      <div className="Home">
        <div className="Pie" style={{ maxWidth: "350px" }}>
          <h6>Macronutrients</h6>
          <Pie data={pieData} />
        </div>

        <div className="Bar" style={{ maxWidth: "350px" }}>
          <h6>Caloires Burned</h6>
          <Bar data={barData} width={320} />
        </div>

        <div className="Line" style={{ maxWidth: "350px" }}>
          <h6>Work Capacity</h6>
          <Line data={lineData}  />
        </div>

        <div className="Bar2" style={{ maxWidth: "350px" }}>
          <h6>Steps Taken</h6>
          <Bar data={secondBarData} />
        </div>
      </div>
    );
  }
}

export default Charts;
