import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { completedExercise, resetExercise } from "../actions"
import { Button } from "shards-react";

const wrapper = {
	display: 'flex',
	flexDirection: 'column',
	alignItems: 'center',
	justifyContent: 'space-around',
	width: "100%",
	height: "500px",
   
	color: '#444',
	border: '1px solid #1890ff',
  };



class Workout extends Component {
	

  
  
    
  handleChange = event => {
    this.setState({value: event.target.value});
  
    
  };

  handleCreateToDo = event => {
    if(event.key === "Enter") {

    const newTodo = {
      userID: 1,
      id: this.state.todos.length + 1,
      title: this.state.value,
      completed: false
    }
    const newTodos = this.state.todos.slice();
    newTodos.push(newTodo);
    this.setState({todos: newTodos, value: ''});
    }
  };

  handleDeleteTodo = todoIdToDelete => event => {
   
    const newTodos = this.state.todos.slice();

    const todoIndexToDelete = newTodos.findIndex(todo => {
      if (todo.id === todoIdToDelete) {
        return true;
	  }	else {
		  return false;
	  }
	  
    });
    newTodos.splice(todoIndexToDelete, 1);
    this.setState({ todos: newTodos });
  };

  handleDeleteSelected = () => {
		const newTodos = this.state.todos.filter(todo => {
			if (todo.completed === true) {
				return false
				
			  } else {
				  return true;
			  }
		})
		this.setState({todos: newTodos});
	  
	};
	 

	
	


  completedToggle = (todoIdToToggle) => (event) => {
	this.props.completedExercise(todoIdToToggle);
	

  }

  completedReset = (resetWorkouts) => {
	  this.props.resetExercise(resetWorkouts);
  }

  completedLineThrough = (todos) => {
	  if(todos.completed === true) {
		  return todos.completed = true;
	  } else {
		return todos.completed = false;
	  }
  };

  
	render() {
		return (

			<section className="todoapp">
	        <div style={wrapper}>
				<Link to="/profile">
				<Button>Go Back To Profile</Button>
				</Link>
    
        
      
		
				<header className="header">
					<h1>Workout List</h1>
        
				</header>
				<TodoList handleDeleteTodo={this.handleDeleteTodo} todos={this.props.todos} completedToggle={this.completedToggle}/>
				<footer className="footer">
					
					{<Button className="clear-completed" onClick={this.completedReset}>Workout Finished!</Button> }
				</footer>
			</div>
			</section>
		);
	}
}

class TodoItem extends Component {
	render() {
		return (
			<li className={this.props.completedLineThrough ? "completed" : ""} >
				<div className="view">
					<input
						className="toggle"
            			type="checkbox"
						checked={this.props.completedLineThrough}
						//onClick={this.props.completedLineThrough}
						onChange={this.props.completedToggle}
					/>
					
					<a href={this.props.workoutVideo} target="_blank"><label>{this.props.title}</label></a>
					
				</div>
			</li>
		);
	}
}

class TodoList extends Component {
	render() {
		return (
			<section className="main">
				<ul className="todo-list">
					{this.props.todos.map(todo => (
						<TodoItem key={todo.id} title={todo.title} completedLineThrough={todo.completed} completedToggle={this.props.completedToggle(todo.id)} workoutVideo={todo.workoutVideo} />
					))}
				</ul>
			</section>
		);
	}
}


const mapStateToProps = (state) => {
	return { todos: state.workouts.workouts[state.workouts.selectedWorkout] }
}
const mapDispatchToProps = {
	completedExercise,
	resetExercise
}
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Workout);