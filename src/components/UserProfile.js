import React, { Component } from "react";
import Charts from "./Charts";
import { Link } from "react-router-dom";
import Macronutrients from "./Macronutrients";
import Calories from "./Calories";
import WorkoutDropdown from "./WorkoutDropdown";
import { Button } from "shards-react";
import { connect } from "react-redux";
import { selectWorkout } from "../actions";
// import FakeUsers from "./FakeUsers"
import "shards-ui/dist/css/shards.min.css";


const userInfo = {
  Age: 24,
  Height: 71,
  Weight: 165
};

class UserProfile extends Component {
  state = {
    selectedWorkout: ""
  };

  switchWorkouts = workoutName => {
    this.props.selectWorkout(workoutName);
  };
  state = { showDummyCharts: false };
  render() {
    return (
      <>
        <div className="userinfo">
          <h1>User Info</h1>
          <h5>Name: {} </h5>
          <h5>Age: {userInfo.Age} </h5>
          <h5>Height: {userInfo.Height} </h5>
          <h5>Weight: {userInfo.Weight} </h5>
        </div>
        <Button
          onClick={() => this.setState({ showDummyCharts: true })}
          className="fullWeekButton"
          size="sm"
        >
          7 days worth of Workout
        </Button>

          <div className="selectedWorkout">
          <p>
            {this.props.selectedWorkout
              ? `Your workout: ${this.props.selectedWorkout}`
              : "You haven't selected a workout for today."}
          </p>
          {!this.props.selectedWorkout && (
            <WorkoutDropdown
              workoutNames={this.props.workouts}
              handleSwitchWorkout={this.switchWorkouts}
            />
          )}
          {this.props.selectedWorkout && (
            <Link to="/workout">
              <Button>Checkout your workout</Button>
            </Link>
          )}
          </div>
        {/* <Calories /> */}


        {this.state.showDummyCharts ? (
          <Charts />
        ) : (
          <h2>Enter your data for your fitness status</h2>
        )}

        
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    selectedWorkout: state.workouts.selectedWorkout,
    workouts: Object.keys(state.workouts.workouts)
  };
};
const mapDispatchToProps = {
  selectWorkout
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserProfile);
