import React, { Component } from "react";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Button
} from "shards-react";


import "shards-ui/dist/css/shards.min.css"
import 'bootstrap/dist/css/bootstrap.min.css';

export default class WorkoutDropdown extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = { open: false };
  }

  
  
  toggle() {
    this.setState(prevState => {
      return { open: !prevState.open };
      
    });
  }
  render() {
    return (
      <div className="workoutDropDown">
  
        <Dropdown open={this.state.open} toggle={this.toggle} group>
          <Button size="sm">Select Workout</Button>
          <DropdownToggle />
          <DropdownMenu>
            {this.props.workoutNames.map(workoutName => {
              return <DropdownItem onClick={() => this.props.handleSwitchWorkout(workoutName)} >{ workoutName }</DropdownItem>
            })}
            
          </DropdownMenu>
        </Dropdown>
        
      </div>
    );
  }
}




