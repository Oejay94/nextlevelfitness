import {
  FETCH,
  FETCH_SUCCESS,
  FETCH_FAIL,
  NUTRITION_FETCH_SUCCESS,
  NUTRITION_FETCH_FAIL,
  fetchDomain,
  latterHalfOfDomain,
  handleJsonResponse,
  apiKeyId,
  foodID
} from "./constants/index";

export const fetchNutrition = search => dispatch => {
  dispatch({
    type: FETCH
  });
  return fetch(`${fetchDomain}${search}${latterHalfOfDomain}${apiKeyId}`)
    .then(handleJsonResponse)
    .then(result => {
      let searchedId = result.hits[0]._id;
      // let name = result.hits[0].item_name;
      fetch(`${foodID}${searchedId}${apiKeyId}`)
        .then(handleJsonResponse)
        .then(nutritionResult => {
          return dispatch({
            type: NUTRITION_FETCH_SUCCESS,
            payload: {
              id: result._id,
              name: result.hits[0].item_name,
              calories: nutritionResult.nf_calories,
              fat: nutritionResult.nf_total_fat,
              carbs: nutritionResult.nf_total_carbohydrate,
              sugars: nutritionResult.nf_sugars,
              portien: nutritionResult.nf_protein,
              sodium: nutritionResult.nf_sodium,
              containsMilk: nutritionResult.allergen_contains_milk,
              containsGluten: nutritionResult.allergen_contains_gluten,
              containsPeanuts: nutritionResult.allergen_contains_peanuts,
              containsEggs: nutritionResult.allergen_contains_eggs,
              containsFish: nutritionResult.allergen_contains_fish,
              containsShellFish: nutritionResult.allergen_contains_shellfish,
              containsTreeNuts: nutritionResult.allergen_contains_tree_nuts,
              containsWheat: nutritionResult.allergen_contains_wheat,
              containsSoyBeans: nutritionResult.allergen_contains_soybeans,
              servingSizeContainer: nutritionResult.nf_servings_per_container,
              servingSizeQT: nutritionResult.nf_serving_size_qty,
              servingSizeUnit: nutritionResult.nf_serving_size_unit
            }
            // .catch(err => {
            //   return Promise.reject(
            //     dispatch({ type: NUTRITION_FETCH_FAIL, payload: err.message })
            //   );
            // })
          });
        });
      return dispatch({
        type: FETCH_SUCCESS,
        payload: result
      })
        // .catch(err => {
        //   return Promise.reject(
        //     dispatch({ type: FETCH_FAIL, payload: err.message })
        //   );
        // })
    });
};
