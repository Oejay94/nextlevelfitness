export const domain = "https://kwitter-api.herokuapp.com";
//export const domain = "http://localhost:3000";

export const jsonHeaders = {
  "Content-Type": "application/json",
  Accept: "application/json"
};

export const handleJsonResponse = res => {
  if (res.ok) {
    return res.json();
  }
  return res.json().then(result => {
    throw result;
  });
};


//nutrition fetch action types
export const FETCH = "FETCH";
export const FETCH_SUCCESS = "FETCH_SUCCESS";
export const FETCH_FAIL = "FETCH_FAIL";
export const NUTRITION_FETCH_SUCCESS = "NUTRITION_FETCH_SUCCESS";
export const NUTRITION_FETCH_FAIL = "NUTRITION_FETCH_FAIL";

//nutritionix api access
export const fetchDomain = "https://api.nutritionix.com/v1_1/search/"
export const  latterHalfOfDomain = "?results=0:20&fields=item_name,brand_name,item_id,nf_calories&"

//foodID
export const foodID = "https://api.nutritionix.com/v1_1/item?id="
export const apiKeyId = "appId=f10c5330&appKey=c117df27fa9f2ca1bcdbf48317f5d194"

