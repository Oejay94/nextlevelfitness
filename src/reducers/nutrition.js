import {
  FETCH,
  FETCH_SUCCESS,
  FETCH_FAIL,
  NUTRITION_FETCH_SUCCESS,
  NUTRITION_FETCH_FAIL
} from "../actions/constants";


const initialState = {
  food: {
    name: '',
    id: 0,
    calories: 0,
    fat: 0,
    carbs: 0,
    sugars: 0,
    portien: 0,
    sodium: 0,
    containsMilk: 0,
    containsGluten: 0,
    containsPeanuts: 0,
    containsEggs: 0,
    containsFish: 0,
    containsShellFish: 0,
    containsTreeNuts: 0,
    containsWheat: 0,
    containsSoyBeans: 0,
    servingSizeContainer: 0,
    servingSizeQT: 0,
    servingSizeUnit: 0
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH:
      return state;
    case FETCH_SUCCESS:
      return state;
    case FETCH_FAIL:
      return initialState;
    case NUTRITION_FETCH_SUCCESS:
      return {
        ...state,
        food: {
          name: action.payload.name,
          id: action.payload.id,
          calories: action.payload.calories,
          fat: action.payload.fat,
          carbs: action.payload.carbs,
          sugars: action.payload.sugars,
          portien: action.payload.portien,
          sodium: action.payload.sodium,
          containsMilk: action.payload.containsMilk,
          containsGluten: action.payload.containsGluten,
          containsPeanuts: action.payload.containsPeanuts,
          containsEggs: action.payload.containsEggs,
          containsFish: action.payload.containsFish,
          containsShellFish: action.payload.containsShellFish,
          containsTreeNuts: action.payload.containsTreeNuts,
          containsWheat: action.payload.containsWheat,
          containsSoyBeans: action.payload.containsSoyBeans,
          servingSizeContainer: action.payload.servingSizeContainer,
          servingSizeQT: action.payload.servingSizeQT,
          servingSizeUnit: action.payload.servingSizeUnit
        }
      }
      case NUTRITION_FETCH_FAIL:
          return initialState
      default:
        return state
  }
};
